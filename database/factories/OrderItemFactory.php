<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product'       =>$faker->text($maxNbChars = 15),
        'quantity'      =>$faker->numberBetween(1, 100),
        'price'            =>$faker->numberBetween(85, 12000),
        'discount'      =>$faker->randomFloat(3,0.5, 1)
    ];
});
