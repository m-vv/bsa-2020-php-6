<?php

use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Buyer::class, 10) ->create()
            ->each( function (\App\Buyer $buyer) {
                $buyer->orders()->saveMany(
                    factory(\App\Order::class, 5)->create(['buyer_id'=>$buyer->id])
                    ->each(function (\App\Order $order) {
                        $order->orderItems()->saveMany(
                            factory(\App\OrderItem::class,7)->create(['order_id'=>$order->id])
                        );
                    }
                    )
                );
            });
    }
}
