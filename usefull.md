#Commands in terminal 
##for user interface
Show list of routers
```bash
docker-compose exec app php artisan route:list
```

Make controller
```bash
docker-compose exec app php artisan make:controller ShopController
```

Make controller for api
```bash
docker-compose exec app php artisan make:controller ShopController --resource  --api 
```
- index for list of model
- store to save data from http request
- show for show model of data (sole item)
- update for updating of model of data
- destroy for delete sole item

## for work with data models
creating models   -m for creating class for migration. Model appears in app  migration in database/migration
first create all models without fields. 
```bash
docker-compose exec app php artisan make:model Product -m
```
We must create new fields in migration then run migration
```bash
docker-compose exec app php artisan migrate
```
in data models we can set name of table, but change in migration too
we can set primary key
$attributes for default value
$guarded will be not available for record via array of data without validation
we can create reset of tables with command
```bash
docker-compose exec app php artisan migrate:reset
```

for  creating of migration --table say where we should make changes.
```bash
docker-compose exec app php artisan make:migration product_seller_migration --table=my_products
```

for creating Fabrics begin with basic objects  they should not consist links on another objects (data models)
```bash
docker-compose exec app php artisan make:factory  SellerFactory --model=Seller
```
1 Buyer
2 Order
3 OrderItem

```bash
docker-compose exec app php artisan make:seeder  SellerSeeder 
```
clear data in data base
```bash
docker-compose exec app php artisan migrate:refresh
```
make seed
```bash
docker-compose exec app php artisan db:seed 
```
make resource to present data from data base

```bash
docker-compose exec app php artisan make:resource  ProductResource
```
