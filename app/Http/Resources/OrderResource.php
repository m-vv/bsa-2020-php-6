<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sumTotal = 0;
        $orderItems=$this->orderItems;
        $itemsArr =[];
        foreach ($orderItems as $orderItem) {
            $item = [];
            $item['productName'] = $orderItem['product'];
            $item['productQty'] = $orderItem['quantity'];
            $item['productPrice'] = $orderItem['price']/100;
            $item['productDiscount'] = $orderItem['discount'];
            $item['productSum'] = round($orderItem['quantity'] * $orderItem['price']/100 *  $orderItem['discount'], 2);
            $sumTotal += $item['productSum'] ;
            array_push($itemsArr, $item);
        }
        return
            ['data'=>
                [
                    'orderId'=>$this->id,
                    'orderDate'=>$this->updated_at,
                    'orderSum' => $sumTotal,
                    'orderItems' => $itemsArr,
                ],
            'buyer'=>
                [
                'buyerFullName' => "{$this->buyer->name} {$this->buyer->surname}",
                'buyerAddress' => "{$this->buyer->country} {$this->buyer->city} {$this->buyer->addressLine}",
                'buyerPhone' => "{$this->buyer->phone}",

                ]
        ];
    }
}
